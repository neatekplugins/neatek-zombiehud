#include <sourcemod>
#include <sdktools>
#include <zombieplague>

#define EVENT_PARAMS Handle event, const char[] name, bool dontBroadcast
#define EVENT_GET_PLAYER GetClientOfUserId(GetEventInt(event, "userid"));
#define VERSION "1.0.0"
#define PLUGIN_NAME "neatek-zombiehud.smx"

int g_modeindex = -1;

enum /*GameModesType*/
{
	GameModes_None 			= -1, 	/** Used as return value when an mode didn't start */
	
	GameModes_Infection,			/** Normal infection */
	GameModes_Multi,				/** Multi infection */
	GameModes_Swarm,				/** Swarm mode */
	GameModes_Nemesis,				/** Nemesis round */
	GameModes_Survivor,				/** Survivor round */
	GameModes_Armageddon			/** Armageddon round */
};

ConVar g_hHudPosX, g_hHudPosY, g_hHudText, g_hHudColor, g_hAuthor;

public Plugin myinfo =
{
	name = PLUGIN_NAME,
	author = "Neatek",
	description = "Simple plugin for HUD zombie-plague",
	version = VERSION,
	url = "https://github.com/neatek/"
};

public void OnPluginStart() {
	g_hAuthor = CreateConVar("sm_plugin_author", "neatek", "Author: Neatek, www.neatek.ru", FCVAR_SPONLY|FCVAR_REPLICATED|FCVAR_NOTIFY);
	//HookEvent("player_spawn", Event_SpawnPlayer);

	g_hHudPosX = CreateConVar("neatek_zhud_x", "0.2", "Позиция текста на экране по горизонтали (от 0.0 до 0.9)", _, true, 0.01, true, 0.9 );
	g_hHudPosY = CreateConVar("neatek_zhud_y", "0.05", "Позиция текста на экране по вертикали (от 0.0 до 0.9)", _, true, 0.01, true, 0.9);
	g_hHudColor = CreateConVar("neatek_zhud_color", "219 155 76", "Цвет текста, обязательно 3 значения через пробел один, пример - 219 155 76");
	g_hHudText = CreateConVar("neatek_zhud_text", "[Создатель] neatek{new}[Группа Вк] vk.com/neatek{new}[Меню сервера на клавишу 'E']{new}[VIP на месяц 100р]{new}[Админка на месяц 250р]", "Здесь можно написать дальшейшие строки для менюшки, {new} - обозначение новой строки");
	
	/** CHECK PLUGIN NAME **/
	char s_path[512];
	BuildPath(Path_SM, s_path, sizeof(s_path), "plugins/%s", PLUGIN_NAME);
	//void PrintToServer(FileExists(s_path), any ...)
	//LogError("exists : %b | %s", FileExists(s_path), s_path);
	if(!FileExists(s_path)) {
		SetFailState("Do not rename Neatek plugin! Rename it back to - '%s'.", PLUGIN_NAME);
	}
	// 0.2 0.05
	RegConsoleCmd("sm_hudmessage", testmessage);
	CreateTimer(5.0, Timer_UpdateHudText, _, TIMER_REPEAT);
	AutoExecConfig(true, "neatek-zombiehud");
}

public void OnMapStart() {
	char s_author[48];
	g_hAuthor.GetString(s_author, sizeof(s_author));
	if(strcmp(s_author, "neatek") != 0) {
		SetFailState("Set ConVar 'sm_plugin_author' back to - 'neatek'! It's a copyright (c) 2017");
	}
}

public void ZP_OnZombieModStarted(int modeIndex) {
	g_modeindex = modeIndex;
	//PrintToChatAll("MODE INDEX: %i", modeIndex);
}

/*native bool ZP_IsPlayerZombie(int clientIndex);
native bool ZP_IsPlayerHuman(int clientIndex);*/

public int CountTeam(bool zombie) {
	int result = 0;
	for(int i=0;i<=GetMaxClients();i++) {
		if(IsCorrectPlayer(i) && GetClientTeam(i) > 1 && IsPlayerAlive(i)) {
			if(zombie) {
				if(ZP_IsPlayerZombie(i))
					result++;
			}
			else {
				if(ZP_IsPlayerHuman(i))
					result++;
			}
		}
	}

	return result;
}

public char ReturnGameMode(char[] buffer, length, index) {
	switch(index) {
		case -1: {
			Format(buffer, length, "Неизвестно");
		}
		case 0: {
			Format(buffer, length, "Обычное заражение");
		}
		case 1: {
			Format(buffer, length, "Мульти заражение");
		}
		case 2: {
			Format(buffer, length, "Куча на кучу");
		}
		case 3: {
			Format(buffer, length, "Немезис появился");
		}
		case 4: {
			Format(buffer, length, "Выживший появился");
		}
		case 5: {
			Format(buffer, length, "Армагеддон!");
		}
	}
}

public Action Timer_UpdateHudText(Handle timer)
{
	char s_buffer[512], s_gamemode[48], s_addit[512];
	ReturnGameMode(s_gamemode,sizeof(s_gamemode),g_modeindex);
	g_hHudText.GetString(s_addit, sizeof(s_addit));
	ReplaceString(s_addit, sizeof(s_addit), "{new}", "\n", true);
	FormatEx(s_buffer, sizeof(s_buffer), "[Режим] %s\n[Люди] %d/%d\n[Зомби] %d/%d\n%s",
		s_gamemode, CountTeam(false), GetMaxClients(), CountTeam(true), GetMaxClients(),s_addit );

	for(int i=0;i<=GetMaxClients();i++) {
		if(IsCorrectPlayer(i) && GetClientTeam(i) > 1) {
			Showhudtext(i, s_buffer);
		}
	}

	return Plugin_Continue;
}


public void Showhudtext(int client, char[] text) {
/*	char arg1[64];
	char arg2[64];
	char arg3[64];
	GetCmdArg(1, arg1, sizeof(arg1));
	GetCmdArg(2, arg2, sizeof(arg2));
	GetCmdArg(3, arg3, sizeof(arg3));*/
	//0.6 0.2 5
	char posX[11], posY[11], hudColor[32];
	g_hHudPosY.GetString(posY, sizeof(posY));
	g_hHudPosX.GetString(posX, sizeof(posX));
	g_hHudColor.GetString(hudColor, sizeof(hudColor));

	int ent = CreateEntityByName("game_text");
	DispatchKeyValue(ent, "channel", "2");
	DispatchKeyValue(ent, "color", hudColor);
	DispatchKeyValue(ent, "color2", "0 0 0");
	DispatchKeyValue(ent, "effect", "0");
	DispatchKeyValue(ent, "fadein", "0.0");
	DispatchKeyValue(ent, "fadeout", "0.0");
	DispatchKeyValue(ent, "fxtime", "0.25"); 		
	DispatchKeyValue(ent, "holdtime", "5.0");
	DispatchKeyValue(ent, "message", text);
	DispatchKeyValue(ent, "spawnflags", "0"); 	
	DispatchKeyValue(ent, "x", posX);
	DispatchKeyValue(ent, "y", posY); 		
	DispatchSpawn(ent);
	SetVariantString("!activator");
	AcceptEntityInput(ent,"display",client);
}

public Action testmessage(int client, int args) 
{ 
	char arg1[64]; 
	char arg2[64]; 
	GetCmdArg(1, arg1, sizeof(arg1)); 
	GetCmdArg(2, arg2, sizeof(arg2)); 
	int ent = CreateEntityByName("game_text"); 
	DispatchKeyValue(ent, "channel", "2");
	DispatchKeyValue(ent, "color", "219 155 76");
	DispatchKeyValue(ent, "color2", "0 0 0");
	DispatchKeyValue(ent, "effect", "0");
	DispatchKeyValue(ent, "fadein", "0.0");
	DispatchKeyValue(ent, "fadeout", "0.0");
	DispatchKeyValue(ent, "fxtime", "0.25"); 		
	DispatchKeyValue(ent, "holdtime", "1.0");
	DispatchKeyValue(ent, "message", "[Режим] -\n[Люди] 0/64\n[Зомби] 0/64\n[Создатель] neatek\n[Группа Вк] vk.com/neatek\n[Меню сервера на клавишу 'E']\n[VIP на месяц 100р]\n[Админка на месяц 250р]");
	DispatchKeyValue(ent, "spawnflags", "0"); 	
	DispatchKeyValue(ent, "x", arg1); 
	DispatchKeyValue(ent, "y", arg2); 
	DispatchSpawn(ent); 
	SetVariantString("!activator"); 
	AcceptEntityInput(ent,"display",client); 
	return Plugin_Handled; 
}

// Function for check is valid or not player
public bool IsCorrectPlayer(int client) {
	if(client > 4096) {
		client = EntRefToEntIndex(client);
	}
		
	if( (client < 1 || client > MaxClients) || !IsClientConnected(client) ||  !IsClientInGame( client ) ) {
		return false;
	}
	
	if(IsFakeClient(client) || IsClientSourceTV(client)) {
		return false;
	}
	
	return true;
}